// (c) 2024 by SwordLord - the coding crew
// This file is part of the 7V application
// and licenced under the AGPLv3

#include "config.h"

#include <fstream>
#include <cpr/cpr.h>
#include <SDL2/SDL_keyboard.h>

#include "./xml/pugixml.hpp"

namespace tv::config
{
    config::config(const std::string& path)
    {
        loadConfig(path);

        std::string version = "0.1.0";
        std::string user_agent{"7v/" + version};

        std::string source = getSource();

        printf("[*] trying to load station list from %s\n", source.c_str());

        cpr::Response r = cpr::Get(cpr::Url{source}, cpr::Header{{"User-Agent", user_agent}});
        if (r.status_code != 200)
        {
            printf("[-] can't open URL %s to update station list. HTTP status code %ld\n", source.c_str(), r.status_code);
            exit(EXIT_FAILURE);
        }

        printf("[*] parsing station list\n");

        pugi::xml_document doc;
        pugi::xml_parse_result result = doc.load_string(r.text.c_str());

        // use load_file for dev purposes
        // pugi::xml_parse_result result = doc.load_file("TV7_default.xspf");

        if(result.status != pugi::xml_parse_status::status_ok)
        {
            printf("[-] xml parsing failed %d, stopping processing of station URLs.\n", result.status);
            return;
        }

        pugi::xpath_query track_query("/playlist/trackList/track");

        pugi::xpath_node_set tracks = track_query.evaluate_node_set(doc);

        printf("[+] stations found in station list: %zu\n", tracks.size());

        printf("[*] checking station list for updated station URLs\n");

        bool local_json_is_dirty{false};

        for (auto node : tracks)
        {
            nlohmann::json::json_pointer stations_ptr("/stations");
            if(!_json.contains(stations_ptr))
            {
                printf("[-] json does not contain /stations\n");
                printf("[-] this means that your local config file is most probably corrupt\n");
                exit(EXIT_FAILURE);
            }

            // iterating json node with stations
            auto& stations = _json[stations_ptr];
            for (auto& elStation : stations)
            {
                std::string name = elStation["name"];
                if(name == node.node().child_value("title"))
                {
                    printf("[+]  station found in local config %s\n", name.c_str());
                    printf("[*]  checking for updated URL\n");

                    std::string location = node.node().child_value("location");
                    if(elStation["url"] != location)
                    {
                        elStation["url"] = location;
                        local_json_is_dirty = true;
                        printf("[+]  URL changed, updating local config\n");
                    }
                    else
                    {
                        printf("[*]  URL is the same, not doing anything\n");
                    }
                }
            }
        }

        if(local_json_is_dirty)
        {
            saveConfig(path);
        }
    }

    void config::loadConfig(const std::string &path)
    {
        printf("[*] loading local config: %s\n", path.c_str());

        std::fstream cf;
        cf.open(path, std::ios::in);
        if(!cf.is_open())
        {
            printf("[-] Can't open the config file for read access: %s\n", path.c_str());
            printf("[+] Generating new config file\n");
            saveConfig(path);
        }

        _json = nlohmann::json::parse(cf);
        cf.close();
    }

    void config::saveConfig(const std::string &path)
    {
        printf("[*] saving local config: %s\n", path.c_str());

        if(_json == nullptr)
        {
            printf("[+] new config generated\n");

            std::string jsonFile =  R"(
{
  "quick_access":
          [
            {
              "kc": "1",
              "pos": 1
            },
            {
              "kc": "2",
              "pos": 33
            },
            {
              "kc": "H",
              "pos": 0
            }
          ],
  "last_station": 0,
  "is_fullscreen": false,
  "source": "https://api.init7.net/tvchannels.xspf?rp=true",
  "stations":
  [
    {
      "sort": 1,
      "name": "SRF 1",
      "url": ""
    },
    {
      "sort": 2,
      "name": "BBC One",
      "url": ""
    }
  ]
})";

            _json = nlohmann::json::parse(jsonFile);
        }

        std::fstream cf;

        try
        {
            cf.open(path, std::ios::out | std::ios::trunc);
        }
        catch (const std::ios::failure& e)
        {
            // TODO probably is a terminal error(?)
            printf("[-] Can't open the config file for write access: %s\n", path.c_str());
            return;
        }

        cf << _json.dump(2);
        cf.close();
    }

    std::string config::getSource()
    {
        if(_json == nullptr)
        {
            return "";
        }

        return _json.value("source", "");
    }

    tv::station::stations config::generateStationsObject()
    {
        tv::station::stations s{};

        nlohmann::json::json_pointer stations_ptr("/stations");
        if(!_json.contains(stations_ptr))
        {
            printf("[-] json does not contain /stations\n");
            exit(EXIT_FAILURE);
        }

        auto& items = _json[stations_ptr];
        for (auto& el : items)
        {
            std::string name = el.value("name", "");
            std::string url = el.value("url", "");

            s.addStation(name, url);
        }

        nlohmann::json::json_pointer bookmarks_ptr("/quick_access");
        if(!_json.contains(bookmarks_ptr))
        {
            printf("[-] json does not contain /quick_access\n");
        }
        else
        {
            auto& bms = _json[bookmarks_ptr];
            for (auto& el : bms)
            {
                std::string code = el.value("kc", "0");
                auto pos = el.value("pos", 0);

                auto kc = SDL_GetKeyFromName(code.c_str());

                s.addBookmark(static_cast<const SDL_KeyCode>(kc), pos);
            }
        }

        return s;
    }

    bool config::isFullscreen()
    {
        if(_json == nullptr)
        {
            return false;
        }

        return _json.value("is_fullscreen", false);
    }
}
