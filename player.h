// (c) 2024 by SwordLord - the coding crew
// This file is part of the 7V application
// and licenced under the AGPLv3

#ifndef INC_7V_PLAYER_H
#define INC_7V_PLAYER_H

#include <string>
#include <map>
#include <vector>

#include "stations.h"

#include "vlcpp/vlc.hpp"

#include <SDL2/SDL.h>
#include <SDL2/SDL_mutex.h>
#include <SDL2/SDL_ttf.h>

namespace tv::player
{
    class player
    {
    public:
        player(tv::station::stations stations, bool is_fullscreen = false);

        void run();
        void tearDown();

    private:

        void display(void* id);
        void* lock(void** p_pixels);
        void unlock(void* id, void* const* p_pixels);

        uint16_t setup();

        void resizeWindow(int32_t windowWidth, int32_t windowHeight);

        void createTextureFromText(SDL_Renderer* renderer, int x, int y, const char* text, TTF_Font* font, SDL_Texture** texture, SDL_Rect* rect);
        // void createStationNameTextures(int x, int y, const char* text);
        void recalculateStationOverlay();

        uint8_t setStation(tv::station::stations::media);

        bool _is_fullscreen{false};

        const uint16_t INITIAL_WINDOW_WIDTH{1600};
        const uint16_t INITIAL_WINDOW_HEIGHT {1024};

        const float ASPECT_RATIO{16.f/9.f};

        // display, where tv stream will be rendered into
        uint16_t _display_width = INITIAL_WINDOW_WIDTH;
        uint16_t _display_height = INITIAL_WINDOW_HEIGHT;

        // SDL context
        SDL_Renderer* _renderer;
        SDL_Texture* _texture;
        SDL_mutex* _mutex;

        // SDL specific clobals
        SDL_Event _event;
        SDL_Window* _window;

        SDL_Rect _rectStationNameSource;
        SDL_Rect _rectStationNameDest;
        SDL_Rect _rectStationBGDest;
        SDL_Texture* _txrStationName;
        SDL_Texture* _txrStationBG;

        TTF_Font* _font;

        // LIBVLC specific globals
        VLC::Instance _vlc;
        VLC::MediaPlayer _mp;
        VLC::Media _m;

        tv::station::stations _stations;

        bool _quit{false};

        uint64_t _station_overlay_fadeout_timer{0};
        uint64_t _mousepointer_fadeout_timer{0};
        const uint64_t FADEOUT_TIME{30};
    };

}

#endif //INC_7V_PLAYER_H
