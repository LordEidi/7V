// (c) 2024 by SwordLord - the coding crew
// This file is part of the 7V application
// and licenced under the AGPLv3

#ifndef INC_7V_CONFIG_H
#define INC_7V_CONFIG_H

#include <string>

#include "nlohmann/json.hpp"
#include "stations.h"

namespace tv::config
{
    class config
    {
    public:
        explicit config(const std::string& path);

        void loadConfig(const std::string& path);
        void saveConfig(const std::string& path);

        std::string getSource();

        bool isFullscreen();

        tv::station::stations generateStationsObject();

    private:

        bool _is_dirty{false};

        nlohmann::json _json = nullptr;
    };
}
#endif //INC_7V_CONFIG_H
