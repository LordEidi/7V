// (c) 2024 by SwordLord - the coding crew
// This file is part of the 7V application
// and licenced under the AGPLv3

#include <cstdio>
#include <cstdlib>
#include <fstream>

#include "config.h"
#include "player.h"

/*
case parser_state::stations:
    {
        media m{elements[0], elements[1]};
        stations.push_back(m);

        printf("Stations: name=%s %s\n", elements[0].c_str(), elements[1].c_str());
    }
    break;
case parser_state::bookmarks:
    {
        // this is a bit of a hassle. We have the key name in the config and need the keycode
        // since that is what we will get during the event loop
        SDL_Keycode kc = SDL_GetKeyFromName(elements[0].c_str());
        // and this is just plain ugly. conversion without any checks if it can be casted
        uint16_t st(std::stoi(elements[1].c_str()));

        bookmarks.insert( bookmarks.end(),  std::pair<SDL_KeyCode, uint16_t>(
                static_cast<const SDL_KeyCode>(kc), st) );

        printf("Bookmarks: Button: %s -> Station: %s\n", elements[0].c_str(), elements[1].c_str());
    }
    break;
*/


// -------------------------------------------------------------------------------------------------

int main(int argc, char* argv[])
{
    printf("[+] reading config\n");

    tv::config::config config{"7v.json"};

    tv::player::player player{config.generateStationsObject(), config.isFullscreen()};

    player.run();

    player.tearDown();

    return EXIT_SUCCESS;
}
//eof