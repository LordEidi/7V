// (c) 2024 by SwordLord - the coding crew
// This file is part of the 7V application
// and licenced under the AGPLv3

#include "stations.h"

namespace tv::station
{
    stations::stations()
    {

    }

    void stations::addStation(std::string &name, std::string &url)
    {
        media m{name, url};
        _stations.push_back(m);
    }

    void stations::addBookmark(SDL_KeyCode keycode, uint16_t position)
    {
        if(!_bookmarks.contains(keycode))
        {
            _bookmarks.insert({keycode, position});
        }
    }

    stations::media stations::nextStation()
    {
        _current_station++;
        stationSanityCheck();

        return getCurrentStation();
    }

    stations::media stations::prevStation()
    {
        _current_station--;
        stationSanityCheck();

        return getCurrentStation();
    }

    stations::media stations::firstStation()
    {
        _current_station = 0;
        stationSanityCheck();

        return getCurrentStation();
    }

    stations::media stations::lastStation()
    {
        _current_station = _stations.size() - 1;
        stationSanityCheck();

        return getCurrentStation();
    }

    stations::media stations::plusNStation(uint16_t plus)
    {
        if(_current_station + plus > _stations.size() - 1)
        {
            _current_station = _stations.size() - 1;
        }
        else
        {
            _current_station += plus;
        }

        stationSanityCheck();

        return getCurrentStation();
    }

    stations::media stations::minusNStation(uint16_t minus)
    {
        if(minus >= _current_station)
        {
            _current_station = 0;
        }
        else
        {
            _current_station -= minus;
        }
        stationSanityCheck();

        return getCurrentStation();
    }

    stations::media stations::gotoBookmarkedStation(SDL_KeyCode keycode)
    {
        if(keycode != SDLK_UNKNOWN && _bookmarks.contains(keycode))
        {
            _current_station = _bookmarks.at(keycode);
            stationSanityCheck();
        }

        return getCurrentStation();
    }

    stations::media stations::getCurrentStation()
    {
        return _stations.at(_current_station);
    }

    void stations::stationSanityCheck()
    {
        if (_current_station < 0)
        {
            _current_station = _stations.size() - 1;
        }
        else if(_current_station >= _stations.size())
        {
            _current_station = 0;
        }
    }
}