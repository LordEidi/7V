// (c) 2024 by SwordLord - the coding crew
// This file is part of the 7V application
// and licenced under the AGPLv3

#include "player.h"

#include <utility>

namespace tv::player
{
    player::player(tv::station::stations stations, bool is_fullscreen) : _stations(std::move(stations)), _is_fullscreen(is_fullscreen)
    {
        setup();
    }

    uint16_t player::setup()
    {
        if (SDL_Init(SDL_INIT_VIDEO | SDL_INIT_TIMER | SDL_INIT_EVENTS ) < 0)
        {
            printf("Could not initialize SDL: %s.\n", SDL_GetError());
            return EXIT_FAILURE;
        }

        printf("[+] setting up SDL\n");

        _window = SDL_CreateWindow(
                "7V",
                SDL_WINDOWPOS_UNDEFINED,
                SDL_WINDOWPOS_UNDEFINED,
                INITIAL_WINDOW_WIDTH, INITIAL_WINDOW_HEIGHT,
                _is_fullscreen ? SDL_WINDOW_FULLSCREEN_DESKTOP : SDL_WINDOW_RESIZABLE);

        if (!_window)
        {
            fprintf(stderr, "[-] Couldn't create window: %s\n", SDL_GetError());
            return EXIT_FAILURE;
        }

        _renderer = SDL_CreateRenderer(_window, -1, 0);
        if (!_renderer)
        {
            fprintf(stderr, "[-] Couldn't create renderer: %s\n", SDL_GetError());
            return EXIT_FAILURE;
        }

        _texture = SDL_CreateTexture(_renderer, SDL_PIXELFORMAT_BGR565, SDL_TEXTUREACCESS_STREAMING, _display_width, _display_height);
        if (!_texture)
        {
            fprintf(stderr, "Couldn't create texture: %s\n", SDL_GetError());
            exit(EXIT_FAILURE);
        }

        _mutex = SDL_CreateMutex();

        printf("[+] setting up font\n");

        // get font and initialise
        TTF_Init();
        _font = TTF_OpenFont("primetime_regular.ttf", 99);
        if (_font == nullptr)
        {
            fprintf(stderr, "[-] Font not found: %s (needs to be in same directory)\n", "primetime_regular.ttf");
            exit(EXIT_FAILURE);
        }

        // station name bg
        // surface can be smaller than destination, it's single colour, distortion is no problem
        SDL_Surface* bg = SDL_CreateRGBSurface(0, 10, 10, 32, 0, 0, 0, 0);
        SDL_SetSurfaceBlendMode(bg, SDL_BLENDMODE_BLEND);

        SDL_FillRect(bg, nullptr, SDL_MapRGB(bg->format, 0, 0, 0));

        _txrStationBG = SDL_CreateTextureFromSurface(_renderer, bg);
        SDL_SetTextureAlphaMod( _txrStationBG, 150);

        printf("[+] setting up VLC\n");

        // Load the VLC engine
        _vlc = VLC::Instance(0, nullptr);
        _vlc.setUserAgent("7V", "7V/0.1.0");
        _vlc.logSet([](int lvl, const libvlc_log_t*, std::string message )
        {
            if(lvl > 3)
            {
                printf("[+] VLC: %s\n", message.c_str());
            }
        });

        setStation(_stations.firstStation());

        // needed to attach SDL
        _mp.setVideoCallbacks([this](void** p_pixels) -> void* {
            return this->lock(p_pixels);
        }, [this](void* id, void* const* p_pixels) {
            this->unlock(id, p_pixels);
        }, [this](void* id){
            this->display(id);
        });

        // Ask VLC for RGBA pixel format
        _mp.setVideoFormat("RV16", _display_width, _display_height, _display_width * 2);

        _mp.play();

        if(_is_fullscreen)
        {
            SDL_ShowCursor(SDL_DISABLE);
        }

        printf("[+] welcome to 7V\n");

        return 0;
    }

    void player::run()
    {
        SDL_KeyCode action{SDLK_UNKNOWN};

        _quit = false;
        while (!_quit)
        {
            action = SDLK_UNKNOWN;

            while (SDL_PollEvent(&_event))
            {
                switch (_event.type)
                {
                    case SDL_QUIT:
                        _quit = true;
                        break;
                    case SDL_KEYDOWN:
                        action = static_cast<SDL_KeyCode>(_event.key.keysym.sym);
                        break;
                    case SDL_MOUSEMOTION:
                        _mousepointer_fadeout_timer = FADEOUT_TIME;
                        SDL_ShowCursor(SDL_ENABLE);
                        break;
                    case SDL_WINDOWEVENT:
                        if (_event.window.event == SDL_WINDOWEVENT_RESIZED )
                        {
                            resizeWindow(_event.window.data1, _event.window.data2);
                        }
                        break;
                }
            }

            switch (action) {
                case SDLK_ESCAPE:
                case SDLK_q:
                    _quit = true;
                    break;
                case SDLK_LEFT:
                    setStation(_stations.prevStation());
                    break;
                case SDLK_RIGHT:
                    setStation(_stations.nextStation());
                    break;
                case SDLK_UP:
                    setStation(_stations.firstStation());
                    break;
                case SDLK_PAGEUP:
                    setStation(_stations.minusNStation(10));
                    break;
                case SDLK_DOWN:
                    setStation(_stations.lastStation());
                    break;
                case SDLK_PAGEDOWN:
                    setStation(_stations.plusNStation(10));
                    break;
                case SDLK_SPACE:
                    _station_overlay_fadeout_timer = FADEOUT_TIME;
                    break;
                default:
                    if(action != SDLK_UNKNOWN)
                    {
                        auto current = _stations.getCurrentStation();
                        auto next = _stations.gotoBookmarkedStation(action);
                        if(next._url != current._url)
                        {
                            setStation(next);
                        }
                    }
                    break;
            }

            SDL_Delay(1000 / 10);

            if(_station_overlay_fadeout_timer > 0)
            {
                _station_overlay_fadeout_timer--;
            }

            if(_mousepointer_fadeout_timer > 0)
            {
                _mousepointer_fadeout_timer--;
            }
        }
    }

    void player::tearDown()
    {
        // spring-cleaning and quit
        _mp.stop();

        // libvlc_media_player_stop(_mp);
        // libvlc_media_player_release(_mp);
        // libvlc_release(_vlc);

        SDL_DestroyTexture(_texture);
        SDL_DestroyTexture(_txrStationName);
        SDL_DestroyTexture(_txrStationBG);

        SDL_DestroyMutex(_mutex);
        SDL_DestroyRenderer(_renderer);
    }

    void player::resizeWindow(int32_t windowWidth, int32_t windowHeight)
    {
        // TODO: check if we should calculate aspect ratio and only react if (something)
        // float aspectRatio = (float)windowWidth / (float)windowHeight;

        // if(aspectRatio != ASPECT_RATIO)
        // {
        //printf("Aspect ratio: %f (%d, %d)\n", (float) windowWidth / (float) windowHeight, display_width, display_height);

        // we always recalculate aspect ration for now
        _display_height = (1.f / ASPECT_RATIO) * windowWidth;
        _display_width = ASPECT_RATIO * windowHeight;

        recalculateStationOverlay();

        //printf("Setting window size to %d, %d, aspect ratio: %f\n", display_width, display_height, (float) windowWidth / (float) windowHeight);
        // }
    }

    void player::createTextureFromText(SDL_Renderer* renderer, int x, int y, const char* text, TTF_Font* font, SDL_Texture** texture, SDL_Rect* rect)
    {
        SDL_Color textColor = {255, 255, 255, 0};

        // we take the wrapped one, since this will correctly interpret \n character
        SDL_Surface* sfcStationName = TTF_RenderUTF8_Blended_Wrapped(font, text, textColor, INITIAL_WINDOW_WIDTH);
        *texture = SDL_CreateTextureFromSurface(renderer, sfcStationName);

        rect->x = x;
        rect->y = y;
        rect->w = sfcStationName->w;
        rect->h = sfcStationName->h;

        SDL_FreeSurface(sfcStationName);
    }

    void player::recalculateStationOverlay()
    {
        int winWidth = 0;
        int winHeight = 0;

        SDL_GetWindowSize(_window, &winWidth, &winHeight);

        auto left = (winWidth - _rectStationNameSource.w) / 2;
        auto top = (winHeight - _rectStationNameSource.h) / 2;

        _rectStationNameDest = SDL_Rect{left, top, _rectStationNameSource.w, _rectStationNameSource.h};
        _rectStationBGDest = SDL_Rect{0, top - 20, winWidth, _rectStationNameSource.h + 40};
    }

    void* player::lock(void** p_pixels)
    {
        int pitch;
        SDL_LockMutex(_mutex);
        SDL_LockTexture(_texture, nullptr, p_pixels, &pitch);

        return nullptr; // Picture identifier, not needed here.
    }

    void player::unlock(void *id, void *const *p_pixels)
    {
        /*
        uint16_t *pixels = (uint16_t *)*p_pixels;

        // example left here for future rendering needs
        int x, y;
        for(y = 10; y < 40; y++) {
            for(x = 10; x < 40; x++) {
                if(x < 13 || y < 13 || x > 36 || y > 36) {
                    pixels[y * display_width + x] = 0xffff;
                } else {
                    // RV16 = 5+6+5 pixels per color, BGR.
                    pixels[y * display_width + x] = 0x02ff;
                }
            }
        }
         */

        SDL_UnlockTexture(_texture);
        SDL_UnlockMutex(_mutex);
    }

    // this is where all the painting to the screen happens
    void player::display(void* id)
    {
        int winWidth = 0;
        int winHeight = 0;

        SDL_GetWindowSize(_window, &winWidth, &winHeight);

        SDL_Rect rect;
        rect.w = _display_width;
        rect.h = _display_height;
        // center the content in the window
        rect.x = (winWidth - _display_width) / 2;
        rect.y = (winHeight - _display_height) / 2;

        SDL_SetRenderDrawColor(_renderer, 0, 0, 0, 255);
        SDL_RenderClear(_renderer);

        // tv signal
        SDL_RenderCopy(_renderer, _texture, nullptr, &rect);

        // station list (and other info), when requested by user
        if(_station_overlay_fadeout_timer > 0)
        {
            // bg
            SDL_RenderCopy(_renderer, _txrStationBG, nullptr, &_rectStationBGDest);

            // text
            SDL_RenderCopy(_renderer, _txrStationName, &_rectStationNameSource, &_rectStationNameDest);
        }

        if(_mousepointer_fadeout_timer == 1)
        {
            SDL_ShowCursor(SDL_DISABLE);
        }

        SDL_RenderPresent(_renderer);
    }

    // switching to the new station, re-rendering the surface with the name of the station in it.
    uint8_t player::setStation(tv::station::stations::media media)
    {
        printf("[*] switching to station: %s\n", media._display_name.c_str());

        _station_overlay_fadeout_timer = FADEOUT_TIME;

        // create new location from given station URL
        _m = VLC::Media(_vlc, media._url, VLC::Media::FromLocation);

        // make sure to have the correct text in the station name texture
        //createStationNameTextures(0, 0, media._display_name.c_str());
        createTextureFromText(_renderer, 0, 0, const_cast<char *>(media._display_name.c_str()), _font, &_txrStationName, &_rectStationNameSource);

        recalculateStationOverlay();

        // change channel on vlc and release media afterwards
        if(_mp == nullptr)
        {
            // Create a media player playing environement
            _mp = VLC::MediaPlayer(_m);

        }

        _mp.stop();
        _mp.setMedia(_m);

        SDL_SetWindowTitle(_window, media._display_name.c_str());

        _mp.play();

        // TODO: return non 0 on error, and take care when having an error
        return 0;
    }

}
