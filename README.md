# 7V

Pronounced "TeeVee". 

SDL and libvlc based player for the Init7 TV stream (HLS).

## Introduction
This is a quick and ugly hack for a friend. This software focuses on simplicity. It contains a hard-coded list of TV stations which can be scrolled through with "left" and "right" on your keyboard, as well as "up" and "down" and "1", "2", etc. for pre-defined bookmarks. Space bar shows the current station name. No further bells and no whistles.

## Last changes
- 2024.08: Moved to cmake:cpm to automatically build (some of the) needed libs.
- 2024.06: There is now an option for fullscreen mode.
- 2024.06: New overlay to show which station you are currently watching.
- 2024.06: Init7 made changes to its HLS station handling. 7V will now download the official stations list on startup and update the stations in the local config if need be.
- 2024.06: Rewrite of config, which is now stored in a json file.

## Constraints
You will need access to the TV7 servers, which you usually get when becoming a customer of Init7.

And you will need sdl2, sdl2_ttf and libvlc already installed. The following libs will be downloaded, compiled and statically linked during the build: libcpr and nlohmann_json.