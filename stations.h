// (c) 2024 by SwordLord - the coding crew
// This file is part of the 7V application
// and licenced under the AGPLv3

#ifndef INC_7V_STATION_H
#define INC_7V_STATION_H

#include <string>
#include <map>
#include <vector>
#include "SDL2/SDL_keycode.h"

namespace tv::station
{
    class stations
    {
    public:
        stations();

        void addStation(std::string& name, std::string& url);
        void addBookmark(SDL_KeyCode keycode, uint16_t position);

        // internal structure for stations
        struct media {
            std::string _display_name;
            std::string _url;
        };

        media nextStation();
        media prevStation();

        media firstStation();
        media lastStation();

        media plusNStation(uint16_t plus);
        media minusNStation(uint16_t minus);

        media gotoBookmarkedStation(SDL_KeyCode keycode);

        media getCurrentStation();

    private:

        // bookmarks are quick_access shortcuts where one keydown will switch to the configured station
        std::map<SDL_KeyCode, uint16_t> _bookmarks = {};

        // station list, read from config, taken from the official Android app (not the complete list)
        std::vector<media> _stations = {};

        int16_t _current_station{0}; // signed, since we can move to -1 (and restart at the top)

        void stationSanityCheck();
    };
}

#endif //INC_7V_STATION_H
